Contributing to 4255.ml
=======================

Four Thousand, Two Hundred and Fifty Five Miles (4255.ml) is a responsive, single-page website. It was created as a server status page for my network of webservers, but is released under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/ "Creative Commons Attribution-ShareAlike 4.0 International License") for anyone to use however they see fit, as long as the license conditions are met.

I'm not much of a front-end developer, and there are probably many opportunities for optimization and improvement. If you choose to make some, I would be grateful to you if you'd share those back so that all can benefit.

Authoritative Repository
------------------------

Source code for 4255.ml is hosted on gitlab.com. The authoritative repository for 4255.ml can be found here:
https://gitlab.com/jaywll/4255.ml

If you wish to contribute code to 4255.ml, you will need an account on gitlab.com. It's a free service, open to all.

There's a mirror hosted on code.jnf.me that can be found here:
https://code.jnf.me/jaywll/4255.ml

By all means feel free to clone or download the project from the mirror, but if you plan to contribute then gitlab.com is the only channel through which to do so.


Contribution Process
--------------------

Once you have a gitlab.com account, here are the steps to making a contribution to 4255.ml's codebase.

### Step One: Fork and Clone Locally ###
From the [project page on gitlab](https://gitlab.com/jaywll/4255.ml), click the "fork" button. This creates a copy of the repository within your own gitlab account. Next, clone your repository locally. 

### Step Two: Branch ###
Create a feature branch, check it out, and hack away!

### Step Three: Commit and Push ###
Commit the changes you've made, and push the feature branch back to origin (your copy of the repository on gitlab.com).

### Step Four: Merge Request ###
Back on your gitlab.com account, within your forked repository, select merge requests and submit a request to merge your feature branch (`username/4255.ml/branchname`) with the project's master branch (`jaywll/4255.ml/master`).

Don't delete your repository yet! Keep an eye on the comments associated with your merge request. There may be further steps requested of you, such as rebasing your feature branch on `jaywll/4255.ml/master`.


GitLab Flow
-----------

TorrentApp uses a cut-down version of [gitlab flow](https://about.gitlab.com/2014/09/29/gitlab-flow/). As per the instructions above, development takes place on feature branches, and these are merged into the `master` branch when that body work represented by the feature branch is complete.

Periodically, the `master` branch is merged into the `release` branch, and these merge commits are tagged with an appropriate version number. These releases occur at the discretion of the project owner: you should not request to merge a feature branch into `release`, and requests to do so will be rejected.


Binary Files and Use-Specific Content
-------------------------------------

The project's repository contains some binary image files, and some of the content of the `index.html` page is specific to the reason I created the page in the first place (a server status page for my own use).

Part of the purpose of the gitlab flow described above is to automatically publish the page to my webservers whenever there is a commit to the `release` branch.

Even though the images I've chosen to use and the content I've included in `index.html` are somewhat specific to me, this workflow means they must remain intact for me to integrate your changes into my repository.

This is somewhat unfortunate and will discourage some people from contributing. I'm open to suggestions for an alternative way to set things up.