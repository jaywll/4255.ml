Four Thousand, Two Hundred and Fifty Five Miles
===============================================

Four Thousand, Two Hundred and Fifty Five Miles (4255.ml) is a single-page, HTML5, response website.

It is named for the distance between Calgary, AB (where I live), and Swansea, Wales (where I am from).

It was built as a "server status" page for my own use, but may be of use to you as an HTML5 template for other purposes (such as a portfolio site), or possibly as a server status page for your own internet servers.


License
-------

4255.ml is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/ "Creative Commons Attribution-ShareAlike 4.0 International License").


Included Files
--------------

### Index.html ###

4255.ml was designed as a single-page site, and as such the huge majority of the content is included in the `index.html` file. All the CSS and JavaScript are included in the `<head>` section.

The page uses [Google Fonts](https://www.google.com/fonts "Google Fonts"), [jQuery](https://jquery.com/ "jQuery"), and [Leaflet.js](http://leafletjs.com/ "Leaflet.js"), all of which are loaded from publicly accessible locations - source files are not included as part of this repository.

*It is very likely that index.html is the only file that will be of use to anyone except me.*

### Images ###

Images used in the page are found within the `www/img` folder.

- `server.png` and `server2.png` are my own work, and are available under the license described above
- `gellygen-*.png`, `jason-*.png` and `jnf-*.png` are screenshots. They are unlikely to be useful to you, but are also available under the license described above
- `swansea.jpg` and `calgary.jpg` are not my work, but were obtained under a license allowing them to be freely used

### Others ###

- `internalservers.php` is a small proxy script to obtain JSON-formatted status of the servers on my internal network. It exists to ensure the "same origin" policy for ajax data is maintained.