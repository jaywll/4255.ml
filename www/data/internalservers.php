<?php
	$context = stream_context_create(array(
		'http' => array (
			'header' => 'Authorization: Basic '.base64_encode('admin@internal:ec=+@/VCEIjcVzSZc$Lr'),
			'ignore_errors' => '1'
		)
	));

	$vms = simplexml_load_string(file_get_contents('https://192.168.0.51/api/vms', false, $context));

	foreach ($vms->vm as $vm) {
		$output[] = array(
			'name' => (string)$vm->name,
			'status' => (string)$vm->status->state
		);
	}

	echo json_encode($output);

?>